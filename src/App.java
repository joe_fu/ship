import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {

        System.out.println("SELECT A PROTOCAL TYPE:");
        System.out.println("1. TCP (DEFAULT)");
        System.out.println("2. UDP");
        try (var in = new Scanner(System.in)) {
            var protocal = 1;
            var selected = in.nextLine();
            if (!selected.isBlank()) {
                protocal = Integer.parseInt(selected);
            }
            System.out.print("PORT (1-65535): ");
            var port = in.nextInt();
            if (port > 0 && port < 65536) {
                switch (protocal) {
                    case 1:
                        try (var s = new ServerSocket(port)) {
                            while (true) {
                                System.out.print("\nWAITING FOR A TCP CONNECTION... ");
                                var incoming = s.accept();
                                System.out.println("CONNECTED!");
                                System.out.println("FROM " + incoming.getRemoteSocketAddress());
                                var r = new TcpEchoHandler(incoming);
                                var t = new Thread(r);
                                t.start();
                            }
                        }
                    case 2:
                        try (var s = new DatagramSocket(port)) {
                            var buf = new byte[1024];
                            while (true) {
                                System.out.print("\nWAITING FOR AN UDP CONNECTION... ");
                                var packet = new DatagramPacket(buf, buf.length);
                                s.receive(packet);
                                System.out.println("CONNECTED!");
                                System.out.println("FROM " + packet.getAddress() + ":" + packet.getPort());
                                var r = new UdpEchoHandler(s, packet);
                                var t = new Thread(r);
                                t.start();
                            }
                        }
                    default:
                        System.out.println("BYE");
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
