import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UdpEchoHandler implements Runnable {

    private DatagramSocket socket;
    private DatagramPacket packet;

    public UdpEchoHandler(DatagramSocket socket, DatagramPacket packet) {
        this.socket = socket;
        this.packet = packet;
    }

    @Override
    public void run() {
        if (packet != null) {
            try {
                var buf = "It works.".getBytes();
                packet = new DatagramPacket(buf, buf.length, packet.getAddress(), packet.getPort());
                socket.send(packet);
            } catch (IllegalArgumentException | IOException e) {
                System.out.println(e);
            }
        }
    }

}
