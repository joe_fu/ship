import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class TcpEchoHandler implements Runnable {

    private Socket s;

    public TcpEchoHandler(Socket s) {
        this.s = s;
    }

    @Override
    public void run() {
        if (s != null) {
            try {
                var outputStream = s.getOutputStream();
                var printWriter = new PrintWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8), true);
                printWriter.println("It works.");
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }

}
